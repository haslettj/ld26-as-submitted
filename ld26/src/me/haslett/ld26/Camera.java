package me.haslett.ld26;

import org.newdawn.slick.geom.Point;
import org.newdawn.slick.geom.Rectangle;

public class Camera  extends Rectangle {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Camera( float x, float y, float width, float height ) {
		super( x, y, width, height );
	}

	public float getScreenX(float worldX){
		return worldX - this.x;
	}
	public float getScreenY(float worldY){
		return worldY - this.y;
	}
	
	public Point getScreenPoint(Point worldPoint){
		return new Point(worldPoint.getX()-this.x, worldPoint.getY()-this.y);
	}
	
	public Rectangle getScreenRectangle(Rectangle rectangle){
		return new Rectangle( rectangle.getX() - this.x, rectangle.getY() - this.y, rectangle.getWidth(), rectangle.getHeight() );
	}
	
	public String getDebug(){
		return "(" + this.x + ", " + this.y + " {" + this.width + ", " + this.height + "}";
	}

}
