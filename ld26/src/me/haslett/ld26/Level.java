package me.haslett.ld26;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import me.haslett.collections.AVLTree;
import me.haslett.collections.MapEntry;

import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.util.ResourceLoader;

public class Level {

	public static final float INITIAL_GRAVITY = 0.4f;
	public static final float INITIAL_FRICTION[] = { 0.5f, 0.5f, 0.5f, 0.0f };
	protected static final float FLOOR = 460;

	protected float gravity;
	protected float friction[];

	protected AVLTree<Integer, Entity> _entities;
	// protected ArrayList<MapEntry<Integer, Entity>> _entities;

	protected Image background;
	protected Talisman[] talismans;
	protected String displayText;

	protected int nextEntityKey = 0;

	protected int spawnBlockX, spawnBlockY; // Starting block location. Player
											// will start standing on this
											// block.

	public Level( Image background ) throws Exception {
		this.background = background;
		this.talismans = new Talisman[] { null, null, null, null };

		_entities = new AVLTree<Integer, Entity>();
		// _entities = new ArrayList<MapEntry<Integer, Entity>>();

		this.gravity = INITIAL_GRAVITY;
		this.friction = INITIAL_FRICTION;

	}

	public float getGravity() {
		return gravity;
	}

	public float getFriction( int blockColor ) {
		float ret = friction[blockColor - 1];

		if ( talismans[Talisman.INDEX_BLUE].getActiviationStatus() != Talisman.BEHAVIOR_DEFAULT && blockColor == Block.COLOR_BIT_BLUE )
			ret = friction[Block.COLOR_BIT_BLACK];
		return ret;
	}

	public Image getBackground() {
		return background;
	}

	public int getSpawnBlockX() {
		return spawnBlockX;
	}

	public void setSpawnBlockX( int x ) {
		spawnBlockX = x;
	}

	public int getSpawnBlockY() {
		return spawnBlockY;
	}

	public void setSpawnBlockY( int y ) {
		spawnBlockY = y;
	}

	public Talisman getTalisman( int index ) {
		return talismans[index];
	}

	public void setTalismans( Talisman[] talismans ) {
		this.talismans = talismans;
	}

	public String getDisplayText() {
		return displayText;
	}

	public void setDisplayText( String value ) {
		displayText = value;
	}

	public int addEntity( Entity e ) throws Exception {
		int key = nextEntityKey;
		// _entities.add( key, new MapEntry<Integer, Entity>( key, e ) );
		_entities.Add( key, e );

		nextEntityKey++;

		return key;
	}

	public void removeEntity( int key ) {
		_entities.Remove( key );

	}

	public Entity getEntity( int index ) {
		return _entities.get( index ).getValue();
	}

	public void render( GameContainer gc, Graphics g, Camera camera, float topBorder ) {

		background.draw( 0 - camera.getX(), 0 - camera.getY() );

		for ( MapEntry<Integer, Entity> map : _entities ) {
			if ( camera.intersects( map.getValue().getWorldCollisionBox() ) ) {
				map.getValue().render( gc, g, camera, topBorder, map.getKey() );
			}
		}

	}

	public ArrayList<MapEntry<Integer, Entity>> CollidesWith( Entity entity ) {
		ArrayList<MapEntry<Integer, Entity>> returnList = new ArrayList<MapEntry<Integer, Entity>>();

		for ( MapEntry<Integer, Entity> map : _entities ) {
			// if ( entity.IntersectsWorld( map.getValue() ) ) {
			if ( map.getValue().IntersectsWorld( entity ) ) {
				returnList.add( map );
			}
		}

		return returnList;
	}

	public static Level loadFromFile( String FileName, SpriteSheet blockSprites, Animation[] blockAnimations, SpriteSheet talismanSprites, Animation[] talismanAnimations, SpriteSheet potatoSprites,
			Animation potatoAnimation ) throws SlickException, Exception {
		InputStream stream = ResourceLoader.getResourceAsStream( FileName );

		BufferedReader reader = new BufferedReader( new InputStreamReader( stream ) );

		Level newLevel = new Level( new Image( reader.readLine() ) );
		newLevel.setSpawnBlockX( Integer.valueOf( reader.readLine() ) );
		newLevel.setSpawnBlockY( Integer.valueOf( reader.readLine() ) );
		newLevel.setDisplayText( reader.readLine() );

		String line;
		int y = 0;
		byte[] buffer;
		int id;
		Talisman t;

		while ( reader.ready() ) {
			line = reader.readLine();
			buffer = line.getBytes();

			for ( int x = 0; x < buffer.length; x++ ) {
				id = buffer[x] - 48;
				if ( id > 0 && id < 5 ) {
					newLevel.addEntity( new Block( x, y, id, blockSprites, blockAnimations ) );
				} else if ( id > 4 && id < 9 ) {
					t = new Talisman( x, y, id - 4, talismanSprites, talismanAnimations );
					t.setIndex( newLevel.addEntity( t ) );
					t.setLevel( newLevel );
				} else if ( id == 9 ) { // POTATO!!!
					t = new Potato( x, y, potatoSprites, potatoAnimation );
					t.setIndex( newLevel.addEntity( t ) );
					t.setLevel( newLevel );
				}

			}
			y++;
		}

		reader.close();
		stream.close();

		return newLevel;
	}
}
