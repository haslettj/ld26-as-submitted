package me.haslett.ld26;

import org.newdawn.slick.Animation;
import org.newdawn.slick.SpriteSheet;

public class Block extends Entity {

	public final static int SIZE = 30;

	public static final int ANIMATION_COUNT = 4;
	public static final int ANIMATION_OFFSET_TO_COLOR = -1;
	public static final int ANIMATION_INDEX_DEFAULT = 0;
	public static final int ANIMATION_INDEX_DEACTIVATED = 1;
	public static final int ANIMATION_INDEX_INCORPOREAL = 2;
	public static final int[] ANIMATION_FRAME_COUNTS = new int[] { 1, 3, 3, 3 };
	public static final int[] ANIMATION_DURATIONS = new int[] { 200, 200, 200, 200 };
	public static final boolean[] ANIMATION_PING_PONG = new boolean[] { true, true, true, true };

	public Block( int x, int y, int color, SpriteSheet sprites, Animation[] animations ) {
		super( x * SIZE, y * SIZE, SIZE, SIZE );

		this.animationSheet = sprites;
		this.animations = animations;

		this.color = color;

		if ( this.color <= ANIMATION_COUNT ) {
			this.currentAnimation = this.color + ANIMATION_OFFSET_TO_COLOR;
		} else {
			this.currentAnimation = 0;
		}
	}

	@Override
	public boolean IntersectsWorld( Entity entity ) {
		if ( this.animations[this.currentAnimation].getFrame() != ANIMATION_INDEX_INCORPOREAL ) {
			return super.IntersectsWorld( entity );
		}
		return false;
	}

}
