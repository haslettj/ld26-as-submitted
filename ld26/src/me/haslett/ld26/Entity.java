package me.haslett.ld26;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;

public abstract class Entity {

	public final static int COLOR_BIT_NONE = 0;
	public final static int COLOR_BIT_BLACK = 1;
	public final static int COLOR_BIT_RED = 2;
	public final static int COLOR_BIT_GREEN = 3;
	public final static int COLOR_BIT_BLUE = 4;

	protected SpriteSheet animationSheet;
	protected Animation[] animations;

	protected int currentAnimation;

	protected int color;

	protected float x, y; // Entity Location (upper left corner)

	protected String _name = "Unknown Entity";
	protected Rectangle hitBox;

	protected Color hitBoxColor = Color.white;

	public Entity( float x, float y, float width, float height ) {
		this.x = x;
		this.y = y;

		hitBox = new Rectangle( 0, 0, width, height );

		color = COLOR_BIT_NONE;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public Rectangle getWorldCollisionBox() {
		return new Rectangle( x + hitBox.getX(), y + hitBox.getY(), hitBox.getWidth(), hitBox.getHeight() );
	}

	public String getName() {
		return _name;
	}

	public void setName( String name ) {
		_name = name;
	}

	public int getColor() {
		return color;
	}

	public void collectTalisman( Talisman talisman ) {

	}

	public void render( GameContainer gc, Graphics g, Camera camera, float topBorder, int displayIndex ) {
//		Color origColor = g.getColor();
		if ( animations != null ) {
			animations[currentAnimation].draw( camera.getScreenX( x ), camera.getScreenY( y ) + topBorder );
		}

		// g.setColor( hitBoxColor );
		// g.draw( camera.getScreenRectangle( getWorldCollisionBox() ) );
		// g.setColor( origColor );

//		if ( displayIndex > -1 ) {
//			g.setColor( hitBoxColor );
//			g.drawString( String.valueOf( displayIndex ), camera.getScreenRectangle( getWorldCollisionBox() ).getX(), camera.getScreenRectangle( getWorldCollisionBox() ).getY() + topBorder );
//			g.setColor( origColor );
//		}
	}

	public void update( Level level ) {
	}

	public boolean IntersectsWorld( Entity entity ) {
		return getWorldCollisionBox().intersects( entity.getWorldCollisionBox() );
	}

	public String getDebug() {
		return "(" + x + ", " + y + ")";
	}
}
