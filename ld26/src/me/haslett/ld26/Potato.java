package me.haslett.ld26;

import org.newdawn.slick.Animation;
import org.newdawn.slick.SpriteSheet;

public class Potato extends Talisman {

	public Potato( int x, int y, SpriteSheet sprites, Animation animations ) {
		super( x, y, 0, sprites, new Animation[] { animations } );

		this._name = "Potato";
		this.currentAnimation = 0;
	}

}
