package me.haslett.ld26;

import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SpriteSheet;

public class Talisman extends Entity {

	public static final int BEHAVIOR_DEFAULT = 0;
	public static final int BEHAVIOR_DEACTIVATE = 1;
	public static final int BEHAVIOR_INCORPOREAL = 2;

	public static final int INDEX_BLACK = 0;
	public static final int INDEX_RED = 1;
	public static final int INDEX_GREEN = 2;
	public static final int INDEX_BLUE = 3;

	public static final int ANIMATION_COUNT = 4;
	public static final int ANIMATION_OFFSET_TO_COLOR = -1;
	public static final int ANIMATION_INDEX_DEFAULT = 0;
	public static final int ANIMATION_INDEX_DEACTIVATED = 1;
	public static final int ANIMATION_INDEX_INCORPOREAL = 2;
	public static final int[] ANIMATION_FRAME_COUNTS = new int[] { 4, 4, 4, 4 };
	public static final int[] ANIMATION_DURATIONS = new int[] { 100, 100, 100, 100 };
	public static final boolean[] ANIMATION_PING_PONG = new boolean[] { false, false, false, false };

	protected int activationStatus;
	protected boolean collected;

	protected Level level;
	protected int myIndex;

	public Talisman( int x, int y, int color, SpriteSheet sprites, Animation[] animations ) {
		super( x * Block.SIZE, y * Block.SIZE, Block.SIZE, Block.SIZE );

		activationStatus = BEHAVIOR_DEFAULT;
		this.color = color;
		this.animationSheet = sprites;
		this.animations = animations;
		this.currentAnimation = color + ANIMATION_OFFSET_TO_COLOR;
		collected = false;
		
		this._name = "Talisman";
	}

	public int getActiviationStatus() {
		return activationStatus;
	}

	public void setActivationStatus( int status ) {
		activationStatus = status;
	}

	public void incrementActivationStatus() {
		activationStatus++;
		if ( activationStatus > 2 )
			activationStatus = 0;
	}

	public void decrementActivationStatus() {
		activationStatus--;
		if ( activationStatus < 0 )
			activationStatus = 2;
	}

	public boolean getIsCollected() {
		return collected;
	}

	public void setIsCollected( boolean value ) {
		collected = value;
	}

	public void setLevel( Level level ) {
		this.level = level;
	}

	public void setIndex( int index ) {
		this.myIndex = index;
	}

	public void renderStatus( int screenX, int screenY, GameContainer gc, Graphics g, Animation animation ) {

		animation.draw( screenX, screenY );

	}

	@Override
	public boolean IntersectsWorld( Entity entity ) {
		if ( super.IntersectsWorld( entity ) && entity.getName() == Player.NAME ) {
			entity.collectTalisman( this );
			level.removeEntity( myIndex );
		}
		return false;
	}

}
